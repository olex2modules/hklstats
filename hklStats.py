
from olexFunctions import OlexFunctions
OV = OlexFunctions()

import os
import sys
import htmlTools
import olex
import olx
import gui


import time
debug = bool(OV.GetParam("olex2.debug", False))
sys.path.append(r"%s/site-packages" % OV.DataDir())

instance_path = OV.DataDir()

try:
  from_outside = False
  p_path = os.path.dirname(os.path.abspath(__file__))
except:
  from_outside = True
  p_path = os.path.dirname(os.path.abspath("__file__"))

l = open(os.sep.join([p_path, 'def.txt'])).readlines()
d = {}
for line in l:
  line = line.strip()
  if not line or line.startswith("#"):
    continue
  d[line.split("=")[0].strip()] = line.split("=")[1].strip()

p_name = d['p_name']
p_htm = d['p_htm']
p_img = eval(d['p_img'])
p_scope = d['p_scope']

OV.SetVar('hklStats_plugin_path', p_path)

from PluginTools import PluginTools as PT

class hklStats(PT):

  def __init__(self):
    super(hklStats, self).__init__()
    self.p_name = p_name
    self.p_path = p_path
    self.p_scope = p_scope
    self.p_htm = p_htm
    self.p_img = p_img
    self.deal_with_phil(operation='read')
    self.print_version_date()
    self.setup_gui()
    OV.registerFunction(self.hklp, True, "hklStats")
    # END Generated =======================================

  def hklp(self, min_I=0, max_I=100, max_y=3, lim_1=0, rat="soi"):

    self.min_I = float(OV.GetParam("%s.plot.min_I" % self.p_scope, min_I))
    self.max_I = float(OV.GetParam("%s.plot.max_I" % self.p_scope, max_I))
    self.max_y = float(OV.GetParam("%s.plot.max_y" % self.p_scope, max_y))

    self.rat = OV.GetParam("%s.plot.type" % self.p_scope, rat)

    self.setup_filters()

    data = []
    self.exclude_min_count = 0
    self.exclude_max_count = 0
    self.exclude_y_large = 0
    self.total_count = 0

    # First Series
    self.x_1 = []
    self.y_1 = []
    self.hkls_1 = []

    # Second Series
    self.x_2 = []
    self.y_2 = []
    self.hkls_2 = []

    self._get_hkl_data()

    self.x_title = "Normalised Intensity"
    if self.rat == "soi":
      def get_y(I, sigma): return sigma / I
      self.y_title = "&sigma;/I"
      self.title = "&sigma;/I vs I for <b>%s</b>" % (OV.FileName())

    elif self.rat == "ios":
      def get_y(I, sigma): return I / sigma
      self.y_title = "I/&sigma;"
      self.title = "I/&sigma; vs I for <b>%s</b>" % (OV.FileName())

    elif self.rat == "ivs":
      def get_y(I, sigma): return sigma
      self.y_title = "&sigma;"
      self.title = "&sigma; <i>vs</i> I for <b>%s</b>" % (OV.FileName())

    for I, sigma, hkl in self.xy:
      self.total_count += 1
      try:
        y = get_y(I, sigma)
      except:
        self.failure_count += 1
        continue
      if self.max_y != 0:
        if y > self.max_y:
          self.exclude_y_large += 1
          continue
      I = (I / self.maxI)
      if I < self.min_I:
        self.exclude_min_count += 1
        continue
      if I > self.max_I:
        self.exclude_max_count += 1
        continue

      if self.andor == "--":
        self.x_1.append(I)
        self.y_1.append(y)
        self.hkls_1.append(hkl)

      else:
        if not self.analyze_indices(hkl):
          self.x_1.append(I)
          self.y_1.append(y)
          self.hkls_1.append(hkl)
        else:
          self.x_2.append(I)
          self.y_2.append(y)
          self.hkls_2.append(hkl)

    self._print_info()
    _ = OV.GetParam("%s.plot_engine" % self.p_scope, "plotly")
    if _ == "plotly":
      self._plot_in_plotly()
    elif _ == "matplotlib":
      self._plot_in_matplotlib()

  def setup_filters(self):
    import operator
    self.ops = {
      "=": operator.eq,
      "!=": operator.ne,
      "<": operator.lt,
      ">": operator.gt,
    }
    self.op_l = []
    t = ""
    self.andor = olx.html.GetValue('andor')
    for item in ["h", "k", "l"]:
      op = olx.html.GetValue('op_%s' % item)
      val = olx.html.GetValue('val_%s' % item)
      self.op_l.append((op, val))
      t += "%s$\,$%s$\,$%s %s " % (item, op, val, self.andor)
    self.filter_text = t[:-4]
    print(t)

  def analyze_indices(self, hkl):
    hkl = hkl.split()
    retVal = False
    i = 0
    for item in self.op_l:
      op = self.ops[self.op_l[i][0]]
      c_val = self.op_l[i][1]
      val = abs(int(hkl[i]))
      if c_val != "even" and c_val != "odd":
        c_val = int(c_val)
        if self.andor == "and":
          if op(val, c_val):
            retVal = True
          else:
            return False
        else:
          if op(val, c_val):
            return True
      else:
        _ = (val % 2) == 0
        if self.andor == "and":
          if _:
            if c_val == "even":
              retVal = True
            else:
              return False
          else:
            if c_val == "even":
              return False
            else:
              retVal = True
        else:  # have the or opeator
          if _:  # the thing is even
            if c_val == "even":
              retVal = True
            else:
              return False

      i += 1
    return retVal
    # if abs(int(hkl[0])) oph _[0][1] and abs(int(hkl[1])) < 5 and abs(int(hkl[2])) < 5:
      # return True
    # return False

  def _print_info(self):
    print("Excluded Reflections, out of a total of %s:" % self.total_count)
    print("-- I < %.1f: %i (%.1f%%)" % (self.min_I, self.exclude_min_count, (self.exclude_min_count / self.total_count) * 100))
    print("-- I > %.1f: %i (%.1f%%)" % (self.max_I, self.exclude_max_count, (self.exclude_max_count / self.total_count) * 100))
    print("-- y > %.1f: %i (%.1f%%)" % (self.max_y, self.exclude_y_large, (self.exclude_y_large / self.total_count) * 100))
    print("-- Failure: %i (%.1f%%)" % (self.failure_count, (self.failure_count / self.total_count) * 100))

  def _get_hkl_data(self):
    hkl = OV.HKLSrc()
    maxI = 0
    failure_count = 0
    xy = []
    if os.path.exists(hkl):
      rFile = open(hkl, 'r').readlines()
      for line in rFile:
        if not line.strip():
          continue
        if "END" in line:
          continue
        hkl_indices = line[0:12].strip()
        I = line[12:20].strip()
        sigma = line[21:28].strip()
        try:
          I = float(I)
          if I > maxI:
            maxI = I
          sigma = float(sigma)
        except:
          failure_count += 1
          continue
        xy.append((I, sigma, line.strip()))
      xy.sort()
    self.xy = xy
    self.maxI = maxI
    self.failure_count = failure_count

  def _plot_in_matplotlib(self):
    try:
      import matplotlib.pyplot as plt
      import numpy as np
    except Exception as err:
      if "No module named" in repr(err):
        selection = olx.Alert("matplotlib not found",
                              """Error: No working matplotlib installation found!.
Do you want to install this now? Olex2 will restart.""", "YN", False)
        if selection == 'Y':
          olex.m("spy.hklstats.pip matplotlib")
          olex.m("restart")
      else:
        print(err)
      return
    
    x = self.x_1
    y = self.y_1
    x2 = self.x_2
    y2 = self.y_2

    label_colour = '#5555bb'

    tfont = {'fontname': 'Bahnschrift', 'fontsize': "16", 'color': label_colour}
    afont = {'fontname': 'Bahnschrift', 'fontsize': "12", 'color': label_colour}

    plt.style.use('seaborn-whitegrid')

    plt.plot(x, y, 'o', color='gray',
             markersize=1, linewidth=0,
             markerfacecolor='white',
             markeredgecolor='gray',
             markeredgewidth=1)
    plt.plot(x2, y2, 'o', color='red',
             markersize=1, linewidth=0,
             markerfacecolor='white',
             markeredgecolor='red',
             markeredgewidth=1)

    plt.text(450, 150,
             self.filter_text,
             horizontalalignment='center',
             verticalalignment='center',
             transform=None,
             color='red')

    plt.xlabel(_format_mathplotlib_text(self.x_title), **afont)
    plt.ylabel(_format_mathplotlib_text(self.y_title), **afont)
    plt.title(_format_mathplotlib_text(self.title), **tfont)
    plt.xticks(fontsize=8, fontname="Bahnschrift", color=label_colour)
    plt.yticks(fontsize=8, fontname="Bahnschrift", color=label_colour)
    plt.grid(True)
    p = os.path.join(OV.FilePath(), "test.png")
    plt.savefig(p)
    olx.Shell(p)
    plt.close()

  def _plot_in_plotly(self):
    try:
      import plotly
      print(plotly.__version__)  # version >1.9.4 required
      from plotly.graph_objs import Scatter, Layout
      import numpy as np
#      import plotly.plotly as py
      import plotly.graph_objs as go
    except:
      selection = olx.Alert("Plot.ly not found",
                            """Error: No working plot.ly installation found!.
Do you want to install this now? Olex2 will restart.""", "YN", False)
      if selection == 'Y':
        olex.m("spy.hklstats.pip plotly")
      return

    data = []
    raw_1 = go.Scatter(
      x=self.x_1,
      y=self.y_1,
      mode='markers',
      marker=dict(size=2,
                  ),
      text=self.hkls_1,
      name='raw_1'
    )
    raw_2 = go.Scatter(
      x=self.x_2,
      y=self.y_2,
      mode='markers',
      marker=dict(size=2,
                  ),
      text=self.hkls_2,
      name='raw_2'
    )

    data.append(raw_1)
    data.append(raw_2)

    annotation = [
      go.layout.Annotation(
        text='Here we go.',
        align='left',
        showarrow=False,
        xref='paper',
        yref='paper',
        x=-10.1,
        y=0.8,
        bordercolor='black',
        borderwidth=1
      )
    ]

    layout = go.Layout(
      font_family="Bahnschrift",
      title=self.title,
      titlefont=dict(
          family='Bahnschrift',
          size=28,
          color='#7f7f7f'
      ),
        xaxis=dict(
            title=self.x_title,
            tickfont=dict(
                family='Bahnschrift',
                size=18,
                color='#7f7f7f'
            ),
            titlefont=dict(
                family='Bahnschrift',
                size=28,
                color='#7f7f7f'
            )
        ),
        yaxis=dict(
            title=self.y_title,
            tickfont=dict(
                family='Bahnschrift',
                size=18,
                color='#7f7f7f'
            ),
            titlefont=dict(
                family='Bahnschrift',
                size=28,
                color='#7f7f7f'
            )
        )
    )

    fig = go.Figure(data=data, layout=layout)

    annotation_x = 0.94
    if self.rat == "soi":
      annotation_y = 0.90
    elif self.rat == "ios":
      annotation_y = 0.25

    fig.add_annotation(text="Excluded Reflections",
                       xref="paper",
                       yref="paper",
                       x=annotation_x,
                       y=annotation_y + 0.035,
                       showarrow=False,
                       font=dict(
                           family="Bahnschrift",
                           size=18,
                           color="#888888"
                       ),)

    l = [
      "Out of a total of %s:" % self.total_count,
      "-- I < %.1f: %i (%.1f%%)" % (self.min_I, self.exclude_min_count, (self.exclude_min_count / self.total_count) * 100),
      "-- I > %.1f: %i (%.1f%%)" % (self.max_I, self.exclude_max_count, (self.exclude_max_count / self.total_count) * 100),
      "-- y > %.1f: %i (%.1f%%)" % (self.max_y, self.exclude_y_large, (self.exclude_y_large / self.total_count) * 100),
      "-- Failure: %i (%.1f%%)" % (self.failure_count, (self.failure_count / self.total_count) * 100)
    ]

    i = 0
    for item in l:
      fig.add_annotation(text=item,
                         xref="paper",
                         yref="paper",
                         x=annotation_x,
                         y=annotation_y - 0.026 * i,
                         showarrow=False,
                         font=dict(
                             family="Bahnschrift",
                             size=16,
                             color="#888888"
                         ),)
      i += 1

    plot_url = plotly.offline.plot(fig, filename='hkl_plot_%s.html' % OV.FileName())


def pip(package):
  import sys
  sys.stdout.isatty = lambda: False
  sys.stdout.encoding = sys.getdefaultencoding()
  import pip
  try:
    from pip import main as pipmain
  except:
    from pip._internal import main as pipmain
  pipmain(['install', '--target=%s\site-packages' % OV.DataDir(), package])
OV.registerFunction(pip, False, "hklstats")


def _gui_manage_evenodd(control):
  if olx.html.GetValue(control) in ["even", "odd"]:
    olx.html.SetValue(control.replace("val_", "op_"), "=")
OV.registerFunction(_gui_manage_evenodd, False, "hklstats")

def _format_mathplotlib_text(text):
  text = text.replace("<b>", "$\mathbf{").replace("</b>", "}$")
  text = text.replace("<i>", "$\mathit{").replace("</i>", "}$")
  text = text.replace("&sigma;", "$\sigma$")
  return text



hklStats_instance = hklStats()
print("OK.")
